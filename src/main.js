// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import FastClick from 'fastclick'
import router from './router'
import App from './App'
import { AlertPlugin } from 'vux'
Vue.use(AlertPlugin)

FastClick.attach(document.body)

Vue.config.productionTip = false

Vue.directive('title', {
  inserted: function(el, binding) {
    document.title = el.dataset.title
  }
})
/* eslint-disable no-new */
new Vue({
  router,
  render: h => h(App)
}).$mount('#app-box')
