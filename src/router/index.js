import Vue from 'vue'
import Router from 'vue-router'
import RegistrationAgreement from '@/components/RegistrationAgreement'
import ServeAgreement from '@/components/ServeAgreement'
import CompensatoryAgreement from '@/components/CompensatoryAgreement'
import guide from '@/components/guide'
import home from '@/components/home'
import processList from '@/components/processList'
import dataAnalysis from '@/components/dataAnalysis'
import paymentMethod from '@/components/paymentMethod'
import listDetail from '@/components/listDetail'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    name: 'HelloWorld',
    component: guide
  }, {
    path: '/registrationagreement',
    name: 'rgreement',
    component: RegistrationAgreement
  }, {
    path: '/serveagreement',
    name: 'serveagreement',
    component: ServeAgreement
  }, {
    path: '/compensatoryagreement',
    name: 'compensatoryagreement',
    component: CompensatoryAgreement
  }, {
    path: '/guide',
    name: 'guide',
    component: guide
  }, {
    path: '/home',
    name: 'home',
    component: home
  }, {
    path: '/processList',
    name: 'processList',
    component: processList
  }, {
    path: '/dataanalysis',
    name: 'dataAnalysis',
    component: dataAnalysis
  }, {
    path: '/paymentmethod',
    name: 'paymentMethod',
    component: paymentMethod
  }, {
    path: '/listdetail',
    name: 'listDetail',
    component: listDetail
  }]
})
