import request from '@/utils/request'

export function guidenext(params) {
  return request({
    baseURL: '',
    url: '/api/user/wxRegister.htm',
    // url: '/user/login',
    method: 'post',
    params
  })
}
export function getdx(params) {
  return request({
    baseURL: '',
    url: '/api/user/h5SendSms.htm',
    // url: '/user/login',
    method: 'post',
    params
  })
}

// 新获取短信 http : //openapi.allinpaycard.cn/allinpay.balance.service/ balance / getSmsSign
export function getsendMsg(params) {
  return request({
    baseURL: '',
    url: '/openapi/allinpay.balance.service/balance/sendMsg',
    // url: '/user/login',
    method: 'get',
    params
  })
}

// 注册加推广
export function registMember(params, data) {
  return request({
    baseURL: '',
    url: '/openapi/allinpay.balance.service/balance/registMember',
    method: 'post',
    params,
    data
  })
}

// 取图片
export function getimages(params) {
  return request({
    baseURL: '',
    url: '/openapi/allinpay.balance.service/balance/getImgVerify',
    method: 'get',
    params
  })
}

// 验证图片
export function yanzhenimage(params) {
  return request({
    baseURL: '',
    url: '/openapi/allinpay.balance.service/balance/imgVerify',
    method: 'get',
    params
  })

}
